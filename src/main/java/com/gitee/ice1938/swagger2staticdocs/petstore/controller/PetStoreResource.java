/*
 *
 *  Copyright 2015 the original author or authors.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *
 */

package com.gitee.ice1938.swagger2staticdocs.petstore.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gitee.ice1938.swagger2staticdocs.petstore.model.Order;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import springfox.petstore.controller.NotFoundException;
import springfox.petstore.repository.MapBackedRepository;

@RestController
@RequestMapping(value = "/stores", produces = APPLICATION_JSON_VALUE)
@Tag(description = "商店的相关接口", name = "商店的相关接口")
public class PetStoreResource {
	
	static StoreData storeData = new StoreData();

	static class StoreData extends MapBackedRepository<Long, Order> {
	}

	@Operation(summary = "按ID查找采购订单", description = "对于有效的响应，请尝试值<=5或>10的整数id。其他值将生成异常")
	@Parameters({ @Parameter(name = "orderId", description = "需要提取的宠物的ID"

			/*
			 * required = true, dataTypeClass = String.class, allowableValues = "range[1,5]"
			 */) })
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "提供的ID无效"),
			@ApiResponse(responseCode = "404", description = "未找到订单") })
	@RequestMapping(value = "/order/{orderId}", method = GET)
	public ResponseEntity<Order> getOrderById(@PathVariable("orderId") String orderId) throws NotFoundException {
		Order order = storeData.get(Long.valueOf(orderId));
		if (null != order) {
			return ResponseEntity.ok(order);
		} else {
			throw new NotFoundException(404, "未找到订单");
		}
	}

	@Operation(summary = "订购宠物")
	@Parameter(name = "order", description = "订购宠物")
	@ApiResponses({ @ApiResponse(responseCode = "400", description = "无效命令") })
	@RequestMapping(value = "/order", method = POST)
	public ResponseEntity<String> placeOrder(Order order) {
		storeData.add(order);
		return ResponseEntity.ok("");
	}

	@Operation(summary = "按ID删除采购订单", description = "对于有效的响应，请尝试值小于1000的整数id。任何超过1000或非整数都将生成API错误")
	@Parameter(name = "orderId", description = "需要删除的订单的ID")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "提供的ID无效"),
			@ApiResponse(responseCode = "404", description = "未找到订单") })
	@RequestMapping(value = "/order/{orderId}", method = DELETE)
	public ResponseEntity<String> deleteOrder(@PathVariable("orderId") String orderId) {
		storeData.delete(Long.valueOf(orderId));
		return ResponseEntity.ok("");
	}
}
