/*
 *
 *  Copyright 2015 the original author or authors.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *
 */

package com.gitee.ice1938.swagger2staticdocs.petstore.controller;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gitee.ice1938.swagger2staticdocs.petstore.model.User;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import springfox.petstore.controller.NotFoundException;
import springfox.petstore.repository.MapBackedRepository;

@Controller
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(description = "关于用户的操作", name = "C.用户")
public class UserController {
	UserRepository userRepository = new UserRepository();

	static class UserRepository extends MapBackedRepository<String, User> {
	}

	@Operation(summary = "创建用户", description = "这只能由登录用户完成。")
	@Parameters(value = { @Parameter(name = "id", description = "主键", example = "2"),
			@Parameter(name = "username", description = "username", example = "username") })
	@RequestMapping(value = "/map",method = POST)
	@ResponseBody
	public ResponseEntity<User> createUserMap(@RequestBody Map<String, Object> user) {

		// userRepository.add(user);
		return new ResponseEntity<User>(HttpStatus.OK);
	}

	@Operation(summary = "创建用户", description = "这只能由登录用户完成。")
	@RequestMapping(method = POST)
	@ResponseBody
	public ResponseEntity<User> createUser(@RequestBody(required = true) User user) {

		userRepository.add(user);
		return ResponseEntity.ok(user);
	}

	@Operation(summary = "创建具有给定输入数组的用户列表")
	@RequestMapping(value = "/createWithArray", method = POST)
	@ResponseBody
	public ResponseEntity<User> createUsersWithArrayInput(@RequestBody User[] users) {
		for (User user : users) {
			userRepository.add(user);
		}
		return new ResponseEntity<User>(HttpStatus.OK);
	}

	@Operation(summary = "创建具有给定输入数组的用户列表")
	@Parameters({ @Parameter(name = "users", description = "用户对象列表") })
	@RequestMapping(value = "/createWithList", method = POST)
	@ResponseBody
	public ResponseEntity<String> createUsersWithListInput(List<User> users) {
		for (User user : users) {
			userRepository.add(user);
		}
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	@Operation(summary = "更新的用户", description = "这只能由登录用户完成。")
	@Parameters({ @Parameter(name = "username", description = "需要删除的名称"),
			@Parameter(name = "user", description = "更新的用户对象") })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "400", description = "提供的用户无效", headers = {
					@Header(name = "X-Rate-Limit-Limit", description = "当前期间允许的请求数"),
					@Header(name = "X-Rate-Limit-Remaining", description = "本期剩余请求数") }),
			@ApiResponse(responseCode = "404", description = "找不到用户", headers = {
					@Header(name = "X-Rate-Limit-Limit", description = "当前期间允许的请求数"),
					@Header(name = "X-Rate-Limit-Remaining", description = "本期剩余请求数") }) })

	@RequestMapping(value = "/{username}", method = PUT)
	@ResponseBody
	public ResponseEntity<String> updateUser(@PathVariable("username") String username, User user) {
		if (userRepository.get(username) != null) {
			userRepository.add(user);
			return new ResponseEntity<String>(HttpStatus.OK);
		}
		return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	}

	@Operation(summary = "删除用户", description = "这只能由登录用户完成。")
	@Parameters({ @Parameter(name = "username", description = "需要删除的名称") })
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "提供的用户名无效"),
			@ApiResponse(responseCode = "404", description = "User not found") })
	@RequestMapping(value = "/{username}", method = DELETE)

	public ResponseEntity<String> deleteUser(@PathVariable("username") String username) {
		if (userRepository.exists(username)) {
			userRepository.delete(username);
			return new ResponseEntity<String>(HttpStatus.OK);
		}
		return new ResponseEntity<String>(HttpStatus.NOT_FOUND);

	}

	@Operation(summary = "按用户名获取用户")
	@Parameters({ @Parameter(name = "username", description = "需要获取的名称。使用user1进行测试。") })
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "提供的用户名无效"),
			@ApiResponse(responseCode = "404", description = "找不到用户") })
	@RequestMapping(value = "/{username}", method = GET)
	public ResponseEntity<User> getUserByName(@PathVariable("username") String username) {
		User user = userRepository.get(username);
		if (null != user) {
			return new ResponseEntity<User>(user, HttpStatus.OK);
		} else {
			throw new NotFoundException(404, "找不到用户");
		}
	}

	@Operation(summary = "登录系统接口")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "提供的用户名或密码无效") })
	@Parameters({ @Parameter(name = "username", description = "用户密码", required = true),
			@Parameter(name = "password", description = "用户密码", required = true) })
	@RequestMapping(value = "/login", method = GET)
	public ResponseEntity<String> loginUser(@RequestParam("username") String username,
			@RequestParam("password") String password) {
		return new ResponseEntity<String>("logged in user session:" + System.currentTimeMillis(), HttpStatus.OK);
	}

	@RequestMapping(value = "/logout", method = GET)
	@Operation(summary = "注销当前登录的用户会话")
	public ResponseEntity<String> logoutUser() {
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
