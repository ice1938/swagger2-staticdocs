package com.gitee.ice1938.swagger2staticdocs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.net.URL;
import java.nio.file.Paths;

import org.junit.Test;

import io.github.swagger2markup.GroupBy;
import io.github.swagger2markup.Language;
import io.github.swagger2markup.MarkupLanguage;
import io.github.swagger2markup.OpenAPI2MarkupConverter;
import io.github.swagger2markup.OpenSchema2MarkupConfig;
import io.github.swagger2markup.config.builder.OpenAPI2MarkupConfigBuilder;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.parser.OpenAPIV3Parser;
import io.swagger.v3.parser.core.models.ParseOptions;
import io.swagger.v3.parser.core.models.SwaggerParseResult;

/**
 * Unit test for simple App.
 */
public class Swagger2MarkupConverterTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(Swagger2MarkupConverterTest.class);

	/**
	 * 生成AsciiDocs格式文档
	 * 
	 * @throws Exception
	 */
	@Test
	public void generateAsciiDocs() throws Exception {
		logger.info("generateAsciiDocs() - start"); //$NON-NLS-1$

		// 输出Ascii格式
		OpenSchema2MarkupConfig config = new OpenAPI2MarkupConfigBuilder()
				.withSwaggerMarkupLanguage(MarkupLanguage.ASCIIDOC)
//				// 生成Markdown格式文档 .withMarkupLanguage(MarkupLanguage.MARKDOWN)
				.withOutputLanguage(Language.EN).withPathsGroupedBy(GroupBy.TAGS).withGeneratedExamples()
				.withoutInlineSchema().build();
		// ./target/asciidoc/generated
		String generatedDir = System.getProperty("generated.asciidoc.directory");
		//generatedDir = ".\\target\\asciidoc\\generated";
		// "classpath:woshare-miniapp.json";//
		String swagger_input_url = System.getProperty("swagger.input.url");
//		Resource re = new ClassPathResource("woshare-miniapp.json");//
//		swagger_input_url = re.getURL().toString();
		ParseOptions options = new ParseOptions();
		options.setResolve(true);
		SwaggerParseResult result = new OpenAPIV3Parser().readLocation(swagger_input_url, null, options);
		OpenAPI swagger = result.getOpenAPI();

		OpenAPI2MarkupConverter converter = OpenAPI2MarkupConverter.from(swagger).withConfig(config).build();
		converter.toFolder(Paths.get(generatedDir));

		logger.info("generateAsciiDocs() - {} ", generatedDir); 
	}

}
