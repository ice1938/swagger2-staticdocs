# swagger2-staticdocs

#### 介绍
基于swagger2markup文档接口设计，用于独立产生静态文档。

 swagger2markup 分三步步进行文档生成：
 
 1. 获得基于swagger.json
 2. 通过swagger.json文档创建asciidoc文件；
 3. 根据asciidoc文件转换成html或pdf；
 
本项目使用思想不同于 spring-swagger2markup-demo方式--自己自足，通过spring-test 启动项目，使用代码访问获得swagger.json的文件：
 spring-swagger2markup-demo 是基于文档生成工具集成在项目中的设计，并且是运行与maven 的test 周期，但是并不是所有的项目都会运行test；首先，获得swagger.json是基于spring-test 进行读取保存文件，其次通过插件swagger2markup-maven-plugin生成asciidoc文件，再次通过asciidoctor-maven-plugin转换成html&pdf(pdf的转换是基于Jruby进行的)。
 
 本项目仅扮演生成文档的角色，swagger.json的文件是直接通过访问url获取的(另一个项目提供)；当然也是在maven 的test周期，因为只有这个周期是可以运行代码的--写一个测试方法生成asciidoc文件，然后使用asciidoctor-maven-plugin 插件进行转换生成html&pdf。这样就不需要swagger2markup-maven-plugin插件，也可以修正swagger2markup汉化不合理的问题，也不需要spring-test。


#### 软件架构
swagger本身设计是用于运行时模拟开发的工具，所以并不是所有信息都是必须的，比如认证信息完全可以独立约定，不随restful 文档说明。
swagger2markup虽说也提供(.md)MARKDOWN的转换，但是转换html&pdf 只能是(.adoc)ASCIIDOC文件.
默认产生的文档参数定义和路径描述是独立的章节，虽然也可以对每个路径追加额外的描述，但是格式结构是相对固定.
固定格式文档基本能满足大多数的项目要求，但是极个别的项目需要或者提供定制的文档模版，这个需要我们后期开发--实际上使用模版引擎才是正经的生成ASCIIDOC文件的方式
maven 使用 profile 方式配置独立属性.

本项目中的petstore 是基于spring-swagger2markup-demo 进行修改（springfox-petstore应该也是基于spring-swagger2markup-demo），引入 <dependency>
	<groupId>io.springfox</groupId>
	<artifactId>springfox-petstore</artifactId>
	<version>2.9.2</version>
</dependency>
仅保留修改的的类；
主要修改内容为进行中文汉化，并力图仅使用ApiImplicitParam 代替ApiParam 进行参数的说明；
其次是尽量使用spring 的提供的对象进行数据的返回定义

#### 使用说明

1. 以petstore为例，运行 mvn clean test -Ppetstore 或 mvn clean test
2. 如果使用自己的项目路径，可以自定义Profile

#### 注解说明

@Api：用在请求的类上，表示对类的说明
    tags="说明该类的作用，可以在UI界面上看到的注解"
    value="该参数没什么意义，在UI界面上也看到，所以不需要配置"
 
@ApiOperation：用在请求的方法上，说明方法的用途、作用
    value="说明方法的用途、作用"
    notes="方法的备注说明"
@ApiResponses：用在请求的方法上，表示一组响应
    @ApiResponse：用在@ApiResponses中，一般用于表达一个错误的响应信息
        code：数字，例如400
        message：信息，例如"请求参数没填好"
        response：抛出异常的类
 
@ApiModel：用于响应类上，表示一个返回响应数据的信息（这种一般用在post创建的时候，使用@RequestBody这样的场景，请求参数无法使用@ApiImplicitParam注解进行描述的时候）
    @ApiModelProperty：用在属性上，描述响应类的属性

@ApiParam和@ApiImplicitParam的功能是相同的，但是@ApiImplicitParam的适用范围更广。在非JAX-RS的场合（比如使用servlet提供HTTP接口），只能使用@ApiImplicitParam进行参数说明，可能是因为接口并不显式展示入参的名称，所以没有地方去使用@ApiParam，只能在接口的方法声明下方写@ApiImplicitParam
eg：
@ApiImplicitParams({
@ApiImplicitParam(name = "status",  value = "需要考虑用于筛选的状态值",paramType="query",  required = true, dataTypeClass = String.class,defaultValue = "available", allowableValues = "available,pending,sold", allowMultiple = true)})
@ApiImplicitParam 作用在方法上，表示单独的请求参数 
参数： 
1. name ：参数名。 
2. value ： 参数的具体意义，作用。 
3. required ： 参数是否必填。 
4. dataType ：参数的数据类型。默认String，其它值dataType="Integer"
5. defaultValue：参数的默认值
6. paramType ：查询参数类型，这里有几种形式：

类型	作用
path	以地址的形式提交数据，请求参数的获取：@PathVariable.eg：getUser/user/admin
query	直接跟参数完成自动映射赋值，请求参数的获取：@RequestParam.eg： getUser?user =admin 
body	以流的形式提交 仅支持POST
header	参数在request headers 提交 ，请求参数的获取：@RequestHeader
form	以form表单的形式提交 仅支持POST @RequestParam获取

对于@ApiImplicitParam的paramType：query、form域中的值需要使用@RequestParam获取， header域中的值需要使用@RequestHeader来获取，path域中的值需要使用@PathVariable来获取，body域中的值使用@RequestBody来获取，否则可能出错；而且如果paramType是body，name就不能是body，否则有问题，与官方文档中的“If paramType is "body", the name should be "body"不符。
当发POST请求的时候，当时接受的整个参数，不论我用body还是query后台都会报Body Missing错误。这个参数和SpringMvc中的@RequestBody冲突，索性我就去掉了paramType，对接口测试并没有影响。
提交的参数是这个对象的一个json，然后会自动解析到对应的字段上去，也可以通过流的形式接收当前的请求数据，但是这个和上面的接收方式仅能使用一个（用@RequestBody之后流就会关闭了）
